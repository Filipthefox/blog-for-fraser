var gulp = require('gulp');
var source = require('vinyl-source-stream');
var browserify = require('browserify');
var watchify = require('watchify');
var babelify = require('babelify');
var notify = require('gulp-notify');
var jshint = require('gulp-jshint');
var flow = require('gulp-flowtype');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
var browserSync = require('browser-sync').create();
var sass = require('gulp-sass');
var neat = require('node-neat').includePaths;
var normalize = require('node-normalize-scss').includePaths;
var reset = require('node-reset-scss').includePath;

gulp.task('build', function(){
    gulp.src('./src/sass/main.scss')
        .pipe(sass({
            includePaths: ['build'].concat(neat, normalize, reset),
            outputStyle: 'compressed'
        }))
        .pipe(gulp.dest('./dist/css'));

    return browserify({
            entries: [
                './src/js/index.jsx'
            ],
            extensions: ['.jsx']
        })
        .transform('babelify', {
            presets: ['es2015', 'react']
        })
        .bundle()
        .pipe(source('./dist/js/main.js'))
        .pipe(gulp.dest('./'));
});

gulp.task('reload', function(){
    return gulp.src('./dist/js/main.js')
        .pipe(browserSync.stream());
});

gulp.task('uglify', function() {
    return gulp.src('./dist/js/main.js')
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(uglify())
        .pipe(gulp.dest('./dist/js/'));
});

gulp.task('lint', function() {
    return gulp.src([
            'gulpfile.js',
            './src/js/*.jsx',
            './src/js/**/*.jsx'
        ])
        .pipe(jshint({
            linter: require('jshint-jsx').JSXHINT,
            esversion: 6
        }))
        .pipe(jshint.reporter('jshint-stylish'))
        .pipe(flow({
            all: false,
            weak: false,
            killFlow: false,
            beep: true,
            abort: true
        }));
});

gulp.task('default', function() {
    browserSync.init({
        server: {
            baseDir: './'
        },
        ghostMode: false
    });

    gulp.watch(['./src/js/*.jsx', './src/js/**/*.jsx'], ['lint']);
    gulp.watch(['./dist/js/main.js'], browserSync.reload);
    gulp.watch(['./dist/css/main.css'], browserSync.reload);
});
