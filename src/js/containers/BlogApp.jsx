/* @flow */
import React from 'react';
import Store from '../store/store';

class BlogApp extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            loading: false
        };
    }

    getChildContext() {
        return {
            store: Store
        };
    }

    componentDidMount() {
        Store.subscribe(() => {
            this.setState({
                loading: Store.getState().loading
            });
        });
    }

    render() {
        return <div>
            { this.props.children }
            <div className="load-wrapper" style={ this.state.loading ? {display: 'block'} : null }>
                <div className="loader"></div>
            </div>
        </div>;
    }
}

BlogApp.childContextTypes = {
    store: React.PropTypes.object.isRequired
};

export default BlogApp;