/* @flow */
import React from 'react';
import { render } from 'react-dom';
import { Router, Route, Link, IndexRoute, hashHistory } from 'react-router';

import BlogApp from './containers/BlogApp';
import Home from './pages/Home';
import PostManage from './pages/PostManage';
import StorageHelper from './helpers/StorageHelper';
import Post from './pages/Post';
import NotFound from './components/NotFound';

StorageHelper.init();

render((
    <Router history={hashHistory}>
        <Route path="/" component={BlogApp}>
            <IndexRoute component={Home} />
            <Route path="create-post" title="Create new post" component={PostManage} />
            <Route path="update-post/:postId" title="Update post" component={PostManage} />
            <Route path="post/:postId" component={Post} />
            <Route path="*" component={NotFound} />
        </Route>
    </Router>
), document.querySelector('#container'));