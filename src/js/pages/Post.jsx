/* @flow */
import React from 'react';
import StorageHelper from '../helpers/StorageHelper';
import { hashHistory, Link } from 'react-router';
import NotFound from '../components/NotFound';
import Heading from '../components/Heading';
import PostControls from '../components/PostControls';

class Post extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            loaded: false,
            title: '',
            content: '',
            date: '',
            error: false
        };
    }

    componentDidMount() {
        StorageHelper.fakeLoad(this.context.store, () => {
            const postData = StorageHelper.getPost(this.props.params.postId);

            if (postData) {
                this.setState({
                    loaded: true,
                    title: postData.title,
                    content: postData.content,
                    date: postData.date
                });
            }
            else {
                this.setState({
                    loaded: true,
                    error: true
                });
            }
        });
    }

    render() {
        if (this.state.error) {
            return <NotFound />;
        }
        else {
            return <div className="panel panel-default">
                <Heading title={"Post " + this.props.params.postId}>
                    | <Link to="/">
                        Go to listing
                    </Link>
                </Heading>
                { this.postBody() }
            </div>;
        }
    }

    postBody() {
        if (this.state.loaded) {
            return <div className="panel-body post-body">
                <h1>{ this.state.title }</h1>
                <span className="date">{ this.state.date }</span>
                <p>{ this.state.content }</p>
                <PostControls id={parseInt(this.props.params.postId)} handleDelete={this.handleDeleteClick} />
            </div>;
        }
        else {
            return <div className="panel-body post-body">
                <p> Loading post... </p>
            </div>;
        }
    }
}

Post.contextTypes = {
    store: React.PropTypes.object.isRequired
};

export default Post;