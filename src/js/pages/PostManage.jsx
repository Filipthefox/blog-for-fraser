/* @flow */
import React from 'react';
import { hashHistory, Link } from 'react-router';
import StorageHelper from '../helpers/StorageHelper';
import ManageControls from '../components/ManageControls';
import NotFound from '../components/NotFound';
import Heading from '../components/Heading';

class PostManage extends React.Component {
    constructor(props, context) {
        super(props, context);

        if (this.props.params.postId) {
            const post = StorageHelper.getPost(this.props.params.postId);

            if (post) {
                this.state = {
                    title: post.title,
                    titleClass: '',
                    content: post.content,
                    contentClass: '',
                    update: true
                };
            }
            else {
                this.state = {
                    update: true,
                    error: true
                };
            }
        }
        else {
            this.state = {
                title: '',
                titleClass: '',
                content: '',
                contentClass: '',
                update: false
            };
        }
    }

    render() {
        if (this.state.update && this.state.error) {
            return <NotFound />;
        }
        else {
            return (
                <div className="panel panel-default">
                    <Heading title={this.props.route.title}>
                        | <Link to="/"> Go to listing </Link>
                    </Heading>
                    <div className="panel-body">
                        <form onSubmit={(e) => this.handleFormSubmit(e)}>
                            <div className="form-group">
                                <label htmlFor="title"> Title </label>
                                <input type="text" id="title" className={"form-control " + this.state.titleClass}
                                       onChange={(e) => this.handleChangeTitle(e)} defaultValue={this.state.title}/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="content"> Content </label>
                                <textarea id="content" className={"form-control " + this.state.contentClass}
                                          onChange={(e) => this.handleChangeContent(e)} defaultValue={this.state.content}/>
                            </div>
                            <ManageControls update={this.state.update} handleDelete={(e) => this.handleDeleteClick(e)} />
                        </form>
                    </div>
                </div>
            );
        }
    }

    handleChangeTitle(e) {
        this.setState({
            title: e.target.value
        });
    }

    handleChangeContent(e) {
        this.setState({
            content: e.target.value
        });
    }

    handleDeleteClick() {
        const del = StorageHelper.removePost(this.props.params.postId);

        if (del) {
            StorageHelper.fakeLoad(this.context.store, () => {
                hashHistory.push('/');
            });
        }
    }

    handleFormSubmit(e) {
        e.preventDefault();

        if (this.state.title === '' || this.state.content === '') {
            if(this.state.title === '') {
                this.setState({
                    titleClass: 'has-error'
                });
            }

            if(this.state.content === '') {
                this.setState({
                    contentClass: 'has-error'
                });
            }
        }
        else {
            StorageHelper.fakeLoad(this.context.store, () => {
                if (this.state.update) {
                    StorageHelper.updatePost(
                        this.state.title,
                        this.state.content,
                        this.props.params.postId
                    );

                    hashHistory.push('/post/' + this.props.params.postId);
                }
                else {
                    const id = StorageHelper.createPost(
                        this.state.title,
                        this.state.content
                    );

                    hashHistory.push('/post/' + id);
                }
            });
        }
    }
}

PostManage.contextTypes = {
    store: React.PropTypes.object.isRequired,
    router: React.PropTypes.object.isRequired
};

export default PostManage;