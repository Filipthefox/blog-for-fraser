/* @flow */
import React from 'react';
import { Link } from 'react-router';
import List from '../components/List';
import Heading from '../components/Heading';

class Home extends React.Component {
    render() {
        return <div className="panel panel-default">
            <Heading title="Awesome blog!" />
            <div className="panel-body">
                <List />
                <Link to="create-post"><span className="btn btn-primary"> Create new post </span></Link>
            </div>
        </div>;
    }
}

export default Home;