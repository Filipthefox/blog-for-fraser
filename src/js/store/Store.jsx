/* @flow */
import { createStore } from 'redux';
import Main from '../reducers/main';

export default createStore(Main);