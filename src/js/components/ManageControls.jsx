/* @flow */
import React from 'react';

class ManageControls extends React.Component {
    constructor(props, context) {
        super(props, context);
    }

    render() {
        return this.controlButtons();
    }

    controlButtons() {
        if (this.props.update) {
            return <div>
                <button className="btn btn-success" type="submit"> Update </button>
                <button className="btn btn-danger pull-right" type="button" onClick={this.props.handleDelete}> Delete </button>
            </div>;
        }
        else {
            return <div>
                <button className="btn btn-success" type="submit"> Submit </button>
            </div>;
        }
    }
}

ManageControls.propTypes = {
    update: React.PropTypes.bool.isRequired,
    handleDelete: React.PropTypes.func.isRequired
};

export default ManageControls;