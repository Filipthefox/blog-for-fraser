/* @flow */
import React from 'react';
import { Link } from 'react-router';

class ListItem extends React.Component {
    render() {
        return <li>
            <p>
                <Link to={"post/" + this.props.id}>{ this.props.postData.title }</Link>
            </p>
            <span className="date">{ this.props.postData.date }</span>
        </li>;
    }
}

ListItem.propTypes = {
    id: React.PropTypes.number.isRequired,
    postData: React.PropTypes.object.isRequired
};

ListItem.contextTypes = {
    router: React.PropTypes.object.isRequired
};

export default ListItem;