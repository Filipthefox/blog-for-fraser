/* @flow */
import React from 'react';

class Heading extends React.Component{
    render() {
        return <div className="panel-heading">
            { this.props.title } { this.props.children }
        </div>;
    }
}

Heading.propTypes = {
    title: React.PropTypes.string.isRequired
};

export default Heading;