/* @flow */
import React from 'react';
import { Link } from 'react-router';

class NotFound extends React.Component{
    render() {
        return (
            <div className="panel panel-default">
                <div className="panel-heading"> Ooooops | <Link to="/"> Go to listing </Link></div>
                <div className="panel-body">
                    <h1> Page not found! </h1>
                    <p> This page / post was not found, please try again later. </p>
                </div>
            </div>
        );
    }
}

export default NotFound;