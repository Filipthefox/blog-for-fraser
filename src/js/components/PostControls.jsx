/* @flow */
import React from 'react';
import { Link } from 'react-router';

class PostControls extends React.Component {
    render() {
        return <div className="post-controls">
            <Link to={"update-post/" + this.props.id}>
                <span className="btn btn-primary">Manage post</span>
            </Link>
        </div>;
    }
}

PostControls.propTypes = {
    id: React.PropTypes.number.isRequired
};

export default PostControls;