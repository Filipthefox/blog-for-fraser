/* @flow */
import React from 'react';
import { Link } from 'react-router';
import StorageHelper from '../helpers/StorageHelper';
import ListItem from '../components/ListItem';

class List extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            postCount: 0,
            posts: null
        };
    }

    componentDidMount() {
        StorageHelper.fakeLoad(this.context.store, () => {
            this.setState({
                postCount: StorageHelper.postCount(),
                posts: StorageHelper.fetchPosts()
            });
        });
    }

    render () {
        return <div>
            <ul>
                { this.getBlogList() }
            </ul>
        </div>;
    }

    getBlogList() {
        if (this.state.posts === null) {
            return <p> Loading posts... </p>;
        }
        else {
            if (this.state.postCount === 0) {
                return <li>
                    <p> You haven't created any posts yet. </p>
                </li>;
            }
            else {
                return Object.keys(this.state.posts).map((id, key) => {
                    return <ListItem postData={this.state.posts[id]} id={parseInt(id)} key={key}/>;
                });
            }
        }
    }
}

List.contextTypes = {
    store: React.PropTypes.object.isRequired,
};

export default List;