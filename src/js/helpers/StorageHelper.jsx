/* @flow */
class StorageHelper {
    static init() {
        if (localStorage.getItem('posts') === null) {
            localStorage.setItem('posts', JSON.stringify({}));
        }
    }

    static postCount() {
        const posts = this.fetchPosts();

        if (Object.keys(posts).length === 0) {
            return 0;
        }
        else {
            return Object.keys(posts).length;
        }
    }

    static getLastPostId() {
        if (localStorage.getItem('lastId') === null) {
            return 0;
        }
        else {
            return parseInt(localStorage.getItem('lastId'));
        }
    }

    static createPost(title, content) {
        const id = this.getLastPostId() + 1;
        let posts = this.fetchPosts();

        posts[id] = {
            title,
            content,
            date: new Date().toDateString()
        };

        this.savePosts(posts);
        localStorage.setItem('lastId', id);

        return id;
    }

    static updatePost(title, content, id) {
        let posts = this.fetchPosts();

        posts[id].title = title;
        posts[id].content = content;

        this.savePosts(posts);
    }

    static removePost(id) {
        let posts = this.fetchPosts();
        let conf = confirm('Are you sure you want to remove this blog?');

        if (conf) {
            delete posts[id];
            this.savePosts(posts);
        }

        return conf;
    }

    static getPost(id) {
        const posts = this.fetchPosts();

        return posts[id] || false;
    }

    static fetchPosts() {
        return JSON.parse(localStorage.getItem('posts'));
    }

    static savePosts(posts) {
        localStorage.setItem('posts', JSON.stringify(posts));
    }

    static fakeLoad(store, cb) {
        store.dispatch({
            type: 'LOADING_START'
        });

        setTimeout(() => {
            store.dispatch({
                type: 'LOADING_STOP'
            });

            cb();
        }, Math.floor(Math.random() * 1000) + 1000);
    }
}

export default StorageHelper;