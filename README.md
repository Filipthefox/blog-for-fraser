# README #

Blog homework!

### What is this repository for? ###

* This repo contains a blog that allows to create, view, update and remove blog posts
* Technologies used were React, Redux, React-router using hashHistory and localStorage as a database, SCSS with bourbon and neat

### How do I get set up? ###

* Clone the repo
* run /npm install (You need to have Node installed, go to nodejs.org to install if you dont) inside the folder
* run /gulp inside the folder. This serves the application on localhost:300x and runs lints and flowtypes
* If you do any changes to the code, run /gulp build, this builds the main js and css files and reloads the browser